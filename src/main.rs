use std::error::Error;

use chrono::{Datelike, DateTime, TimeZone, Utc};
use serde::Deserialize;
use std::collections::{BTreeMap};

#[derive(Hash, Eq, PartialEq, Ord, PartialOrd, Copy, Clone)]
struct Day(u32, u32, u32);

impl Day {
    pub fn year(&self) -> u32 {
        self.0
    }

    pub fn month(&self) -> u32 {
        self.1
    }

    pub fn day(&self) -> u32 {
        self.2
    }
}

#[derive(Deserialize, Debug)]
struct ExposureRow {
    #[serde(rename = "s")]
    start_timestamp: u64,

    #[serde(rename = "d")]
    duration_in_seconds: u64,

    #[serde(rename = "r")]
    risk_level: f64,
}

impl ExposureRow {
    fn as_date(&self) -> DateTime<Utc> {
        // 2208988800 = Number of seconds to fill the gap between
        // UNIX timestamp (1/1/1970) and NTP timestamp (1/1/1900)
        Utc.timestamp((self.start_timestamp - 2208988800) as i64, 0)
    }

    fn as_day(&self) -> Day {
        let date = self.as_date();
        Day(date.year() as u32, date.month(), date.day())
    }
}

#[derive(Deserialize, Debug)]
struct ClusterFileItem {
    #[serde(rename = "ltid")]
    temporary_location_id: String,

    #[serde(rename = "exp")]
    exposures: Vec<ExposureRow>,
}

#[derive(Deserialize, Debug)]
struct ClusterFileIndex {
    #[serde(rename = "i")]
    iteration: u64,

    #[serde(rename = "c")]
    prefixes: Vec<String>,
}

/// Get the index of cluster reports
fn get_cluster_index(url: &str) -> Result<ClusterFileIndex, Box<dyn Error>> {
    let response = reqwest::blocking::get(format!("{}/clusterIndex.json", url))?.json()?;
    Ok(response)
}

/// Get the clusters list for a given prefix and iteration
fn get_cluster_file_item(url: &str, iteration: u64, prefix: &str) -> Result<Vec<ClusterFileItem>, Box<dyn Error>> {
    let response = reqwest::blocking::get(format!("{}/{}/{}.json", url, iteration, prefix))?.json()?;
    Ok(response)
}



fn main() {
    let args = std::env::args().collect::<Vec<String>>();
    let url = match args.get(1){
        None => "https://s3.fr-par.scw.cloud/clea-batch/v1", // "http://localhost:8000/v1"
        Some(s) => s.as_str()
    }.to_string();

    println!("Using base URL {}", url);

    let index = match get_cluster_index(&url) {
        Ok(index) => index,
        Err(e) => {
            eprintln!("Failed to load file index !\nError: {:?}", e);
            return;
        }
    };

    let mut list = vec![];
    for prefix in &index.prefixes {
        let mut items = match get_cluster_file_item(&url, index.iteration, prefix) {
            Ok(items) => items,
            Err(e) => {
                eprintln!("Failed to load cluster items!\nError: {:?}", e);
                return;
            }
        };

        list.append(&mut items);
    }

    // Process data by day of exposure now
    let mut  map  = BTreeMap::new();
    for item in &list {
        for row in &item.exposures {
            let date = row.as_day();
            if !map.contains_key(&date) {
                map.insert(date, vec![]);
            }

            map.get_mut(&date).unwrap().push(row.risk_level);
        }
    }

    println!("Iteration number: {}", index.iteration);

    // Display the statistics to the user
    for (day, values) in map {
        println!("On {}/{}/{} : {}",
                 day.day(), day.month(), day.year(),
                 values.iter().map(|f|f.to_string()).collect::<Vec<String>>().join(" "));
    }
}
